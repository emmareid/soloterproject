package um.fds.agl.ter22.forms;

import um.fds.agl.ter22.entities.UserTER;

public class SupervisorForm extends UserTER {
    private String firstName;
    private long id;
    private String lastName;

    private Long projectId;

    public SupervisorForm(long id, String firstName, String lastName, long projectId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
        this.projectId = projectId;
    }

    public void setProjectId(){this.projectId = projectId;}

    public Long getProjectId(){return projectId;}

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
