package um.fds.agl.ter22.entities;

public class Supervisor extends UserTER {
    public Long projectId;

    public Supervisor(){}
    public Supervisor(String firstName, String lastName, String password, Long projectId) {
        super();
    }

    public Supervisor(String firstName, String lastName, Long projectId) {
        super();
        String[] roles={"ROLE_SUPERVISOR"};
        projectId = this.projectId;
    }


    @Override
    public String toString() {
        return "Supervisor{" +
                "firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", projectId='"+ getProject() + '\'' +
                '}';
    }


    private Long getProject() {return projectId;}
    private void setProject(Long projectId) {this.projectId = projectId;}




}
