package um.fds.agl.ter22.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import um.fds.agl.ter22.entities.TERManager;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.services.TeacherService;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumingThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class TeacherControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    TeacherService ts;

    @Captor
    ArgumentCaptor<Teacher> captor = ArgumentCaptor.forClass(Teacher.class);

    //    @Test
//    @WithMockUser(username = "Chef", roles = "MANAGER")
//    void addTeacherGet() throws Exception {
//        MvcResult result = mvc.perform(get("/addTeacher"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType("text/html;charset=UTF-8"))
//                .andExpect(view().name("addTeacher"))
//                .andReturn();
//    }
    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostNonExistingTeacherMOCK() throws Exception {
        //WITH MOCK TEACHER SERVICE
        assertTrue(ts.getTeacher(10l).isEmpty());
        Teacher newTeacher = new Teacher("Anne-Marie", "Kermarrec", new TERManager("Le", "Chef", "password", "ROLE_MANAGER"));
        newTeacher.setId(10l);
        when(ts.getTeacher(anyLong())).thenReturn(Optional.of(newTeacher));
        assertFalse(ts.getTeacher(10l).isEmpty());
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void assumingThatAddTeacherPostNonExistingTeacherMOCK(){
        //WITH MOCK TEACHER SERVICE
        Teacher newTeacher = new Teacher("Anne-Marie", "Kermarrec", new TERManager("Le", "Chef", "password", "ROLE_MANAGER"));
        when(ts.getTeacher(anyLong())).thenReturn(Optional.of(newTeacher));
        assumingThat(ts.getTeacher(10l).isEmpty(), ()-> {
                    newTeacher.setId(10l);
                    ts.saveTeacher(newTeacher);
                }
        );
        assertFalse(ts.getTeacher(10l).isEmpty());
    }

    //i need to come back to this one!
    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostExistingTeacher() throws Exception{
        //assume this teacher doesn't exist
        Teacher teacher = new Teacher("Anne-Marie", "Kermarrec", new TERManager("Le", "Chef", "mdp", "ROLE_MANAGER"));

        when(ts.saveTeacher(any(Teacher.class))).thenReturn(teacher);

        assumingThat(ts.getTeacher(9l).isEmpty(), ()-> {
            MvcResult result = mvc.perform(post("/addTeacher")
                            .param("firstName", "Anne-Marie")
                            .param("lastName", "Kermarrec")
                            .param("id", "10")
                    )
                    .andExpect(status().is3xxRedirection())
                    .andReturn();

        });

    }

}
